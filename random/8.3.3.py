from scipy import stats
import random

random.seed(12345)

fMax = 2.0736

def T(Y):
    return fMax

def R(Y):
    return random.uniform(0,1)

def NextYFromR():
    return random.uniform(0,1)

def F(Y):
    return 60 * Y * Y * Y * (1 - Y) *  (1 - Y)

def NextSample():
	while (True):
		fNextY = NextYFromR()
		fNextU = random.uniform(0,1)
		if (fNextU <= F(fNextY) / T(fNextY)):
			return fNextY

Ys = []
for i in range(10000):
    Ys.append(NextSample())

import matplotlib.pyplot as plt

plt.rcParams[u'font.sans-serif'] = ['simhei']
plt.rcParams['axes.unicode_minus'] = False

plt.hist(x = Ys, # 指定绘图数据
    bins = 30, # 指定直方图中条块的个数
    range = (0,1),
    color = 'steelblue', # 指定直方图的填充色
    edgecolor = 'black' # 指定直方图的边框色
)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
# 添加x轴和y轴标签
plt.xlabel('Ys',fontsize=20)
plt.ylabel('频数',fontsize=20)
# 添加标题
plt.title('Beta(4,3)直方图',fontsize=20)
# 显示图形
plt.show()