import pandas as pd
from pandas import DataFrame
import matplotlib.pyplot as plt

plt.rcParams[u'font.sans-serif'] = ['simhei']
plt.rcParams['axes.unicode_minus']=False

# 读取xls（绝对路径）
data = pd.read_excel('./input/datas/cep.xlsx')
#查看所有的值
print(data.values)

plt.hist(x = data['落点偏差'].values, # 指定绘图数据
    bins = 30, # 指定直方图中条块的个数
    range = (30,270),
    color = 'steelblue', # 指定直方图的填充色
    edgecolor = 'black' # 指定直方图的边框色
)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
# 添加x轴和y轴标签
plt.xlabel('落点偏差',fontsize=20)
plt.ylabel('频数',fontsize=20)
# 添加标题
plt.title('落点偏差直方图',fontsize=20)
# 显示图形
plt.show()



