from scipy import stats
import random
import math

random.seed(12345)

def InverseF(U):
	if(U < 1.0/2.0 ):
		return math.sqrt(2 * U)
	return  2 - math.sqrt(2 * (1 - U))

def NextSample():
    fNextU = random.uniform(0,1)
    return InverseF(fNextU)

Ys = []
for i in range(100000):
    Ys.append(NextSample())

import matplotlib.pyplot as plt

plt.rcParams[u'font.sans-serif'] = ['simhei']
plt.rcParams['axes.unicode_minus'] = False

plt.hist(x = Ys, # 指定绘图数据
    bins = 30, # 指定直方图中条块的个数
    range = (0,2),
    color = 'steelblue', # 指定直方图的填充色
    edgecolor = 'black' # 指定直方图的边框色
)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
# 添加x轴和y轴标签
plt.xlabel('Ys',fontsize=20)
plt.ylabel('频数',fontsize=20)
# 添加标题
plt.title('三角分布直方图',fontsize=20)
# 显示图形
plt.show()